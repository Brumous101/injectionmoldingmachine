/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package injectionmoldingmachine;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.material.Material;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;

/**
 * @author Kyle Johnson
 * @author Christopher McCormick
 */
public class Objects extends AbstractAppState {

    
    public Spatial makeHopper(RigidBodyControl hopperRigid, Node noninteractable, BulletAppState bulletAppState, Spatial hopper) {

        hopper.setLocalTranslation(2.0f, 0.5f, 0.0f);
        hopper.rotate(0f, -90 * FastMath.DEG_TO_RAD, 0f);

        //setup physics
        CollisionShape pelletShape = CollisionShapeFactory.createMeshShape(hopper);
        hopperRigid = new RigidBodyControl(0);
        hopper.addControl(hopperRigid);
        //hopper.getControl(RigidBodyControl.class).setFint testriction(0.6f);
        noninteractable.attachChild(hopper);
        bulletAppState.getPhysicsSpace().add(hopperRigid);
        return hopper;
    }
    
    protected Spatial makeMoldsAndPlates(RigidBodyControl moldsAndPlatesRigid, Node noninteractable, BulletAppState bulletAppState,Spatial moldsAndPlates) {
        
        moldsAndPlates.setLocalTranslation(0.0f, -1.0f, 0.0f);
        moldsAndPlates.rotate(90 * FastMath.DEG_TO_RAD,180 * FastMath.DEG_TO_RAD, 0f);
                            
        CollisionShape moldsAndPlatesShape = CollisionShapeFactory.createMeshShape(moldsAndPlates);
        moldsAndPlatesRigid = new RigidBodyControl(0);
        moldsAndPlates.addControl(moldsAndPlatesRigid);
        //frontGate.getControl(RigidBodyControl.class).setFriction(0.6f);
        noninteractable.attachChild(moldsAndPlates);
        bulletAppState.getPhysicsSpace().add(moldsAndPlatesRigid);
        return moldsAndPlates;
    }  
    
    protected Spatial makePlasticPellet(float num1, float num2, float num3, Spatial pellet, RigidBodyControl pelletRigid, Node noninteractable, BulletAppState bulletAppState) {

        pellet.setLocalTranslation(num1, num2, num3);
        pellet.setLocalScale(5f); //change size to view easier during development
        //pellet.rotate(90 * FastMath.DEG_TO_RAD,180 * FastMath.DEG_TO_RAD, 0f);
        
        CollisionShape pelletShape = CollisionShapeFactory.createMeshShape(pellet);
        pelletRigid = new RigidBodyControl(1.5f);
        pellet.addControl(pelletRigid);
        pellet.getControl(RigidBodyControl.class).setFriction(0.6f);
        noninteractable.attachChild(pellet);
        bulletAppState.getPhysicsSpace().add(pelletRigid);
        
        return pellet;
    }    

}


/*  UNUSED CODE

        Spatial frontGate = assetManager.loadModel("Textures/Gate.obj");
        obj.makeFrontGate(frontGateRigid, interactable, bulletAppState, frontGate, frontGatePath1, frontGateMotionControl1, frontGatePath2, frontGateMotionControl2);
                
    protected Spatial makeFrontGate(RigidBodyControl frontGateRigid, Node interactable, BulletAppState bulletAppState,Spatial frontGate, MotionPath frontGatePath1, MotionEvent frontGateMotionControl1, MotionPath frontGatePath2, MotionEvent frontGateMotionControl2) {

        frontGate.setLocalTranslation(0.0f, -1.0f, 0.5f);
        frontGate.rotate(180 * FastMath.DEG_TO_RAD, 180 * FastMath.DEG_TO_RAD, 180 * FastMath.DEG_TO_RAD);
            
        CollisionShape frontGateShape = CollisionShapeFactory.createMeshShape(frontGate);
        frontGateRigid = new RigidBodyControl(0);
        frontGate.addControl(frontGateRigid);
        //frontGate.getControl(RigidBodyControl.class).setFriction(0.6f);
        interactable.attachChild(frontGate);
        bulletAppState.getPhysicsSpace().add(frontGateRigid);
        
        return frontGate;

    }

    protected MotionPath initFrontGatePath1(Spatial frontGate, MotionPath frontGatePath1, MotionEvent frontGateMotionControl1) {
    
        //frontGate OPEN
        frontGatePath1 = new MotionPath();
        frontGatePath1.addWayPoint(new Vector3f(0, -1, 0.5f));
        frontGatePath1.addWayPoint(new Vector3f(-1, -1, 0.5f));
            //path1.enableDebugShape(assetManager, rootNode);

        frontGateMotionControl1 = new MotionEvent(frontGate, frontGatePath1);
        frontGateMotionControl1.setDirectionType(MotionEvent.Direction.PathAndRotation);
        frontGateMotionControl1.setInitialDuration(10f);
        frontGateMotionControl1.setSpeed(15f);        
        
        return frontGatePath1;
    }
        
    
    protected MotionPath initFrontGatePath2(Spatial frontGate, MotionPath frontGatePath2, MotionEvent frontGateMotionControl2) {
    
        //frontGate CLOSE
        frontGatePath2 = new MotionPath();
        frontGatePath2.addWayPoint(new Vector3f(-1, -1, 0.5f));
        frontGatePath2.addWayPoint(new Vector3f(0, -1, 0.5f));

        frontGateMotionControl2 = new MotionEvent(frontGate, frontGatePath2);
        frontGateMotionControl2.setDirectionType(MotionEvent.Direction.PathAndRotation);
        frontGateMotionControl2.setInitialDuration(10f);
        frontGateMotionControl2.setSpeed(15f);        
        
        return frontGatePath2;
    }



//FOR Main.java:
Objects obj1 = new Objects();
                    Objects obj2 = new Objects();
                    Spatial pellet = assetManager.loadModel("Textures/Single_ABS_Pink.obj");


                    noninteractable.attachChild(obj1.makePlasticPellet(2.0f, 1.0f, 0.0f, pellet, pelletRigid, noninteractable, bulletAppState));
                    noninteractable.attachChild(obj2.makePlasticPellet(2.1f, 1.0f, 0.0f, pellet, pelletRigid, noninteractable, bulletAppState));
                    noninteractable.attachChild(obj2.makePlasticPellet(2.1f, 1.2f, 0.1f, pellet, pelletRigid, noninteractable, bulletAppState));
                    noninteractable.attachChild(obj2.makePlasticPellet(1.9f, 1.0f, 0.0f, pellet, pelletRigid, noninteractable, bulletAppState));
                    noninteractable.attachChild(obj2.makePlasticPellet(1.9f, 1.2f, 0.1f, pellet, pelletRigid, noninteractable, bulletAppState));
                    noninteractable.attachChild(obj2.makePlasticPellet(2.0f, 1.0f, 0.1f, pellet, pelletRigid, noninteractable, bulletAppState));
                    noninteractable.attachChild(obj2.makePlasticPellet(2.1f, 1.0f, 0.1f, pellet, pelletRigid, noninteractable, bulletAppState));
                    noninteractable.attachChild(obj2.makePlasticPellet(2.1f, 1.2f, 0.2f, pellet, pelletRigid, noninteractable, bulletAppState));
                    noninteractable.attachChild(obj2.makePlasticPellet(1.9f, 1.0f, 0.1f, pellet, pelletRigid, noninteractable, bulletAppState));
                    noninteractable.attachChild(obj2.makePlasticPellet(1.9f, 1.2f, 0.2f, pellet, pelletRigid, noninteractable, bulletAppState));



*/